
class Z64RotationValues:
	struct = ['>h']
	def __init__(self, rotationValue):
		self.rotationValue = rotationValue

class Z64AnimationHeader:
	struct = ['>h', '>B', '>BBB', '>B', '>BBB', '>h']
	def __init__(self, numFrames, rotValuebank, rotValueOffset, rotValuebank, rotIndexOffset, limit):
		self.numFrames = numFrames
		self.rotValuebank = rotValuebank
		self.rotValueOffset = rotValueOffset
		self.rotValuebank = rotValuebank
		self.rotIndexOffset = rotIndexOffset
		self.limit = limit

