import os
import sys
import re
import struct
import time
from math import *
from struct import unpack_from, pack

from import_z64 import *

import maya.cmds as cmds
import maya.mel as mel
import maya.api.OpenMaya as OpenMaya
import maya.OpenMayaUI as OpenMayaUI
import maya.OpenMayaMPx as OpenMayaMPx
from maya.OpenMaya import MFnMesh
from maya.OpenMaya import MColor
from PySide.QtCore import *
from maya.OpenMaya import MVector as Vector
from maya.OpenMaya import MMatrix as Matrix

PluginName = "Zelda 64 Reader"
Version = "0.67"
settings = None
fpath = ""
fname = ""
AnimtoPlay = 1
ExternalAnims = False
rootObject = None

def MayaSafeName(input):
	output = re.sub('[\']','',input)
	output = re.sub('[. \-#]','_',output)
	while output.find("__")>-1:
		output = output.replace("__", "_")
	return output

class Zelda64Settings:
	# ImportAnimAs: 1 = Trax Clips, 0 = Timeline keyframes, separated by AnimFrameBuffer frames.
	def __init__(self, ExtractTextures = True, ImportTextures = True, ImportBones = True, loadOtherSegments = True, enableMatrices = True, enableTexMirror = False, ImportAnim = True, UseAsMajoraAnim = True, ImportAnimAs = 1, ImportAnimsAll = False, AnimToImport = 1, AnimFrameBuffer = 10):
		self.ExtractTextures = int(ExtractTextures)
		self.ImportTextures = int(ImportTextures)
		self.ImportBones = int(ImportBones)
		self.loadOtherSegments = int(loadOtherSegments)
		self.enableMatrices = int(enableMatrices)
		self.enableTexMirror = int(enableTexMirror)
		self.ImportAnim = int(ImportAnim)
		self.ImportAnimAs = int(ImportAnimAs)
		self.ImportAnimsAll = int(ImportAnimsAll)
		self.AnimToImport = int(AnimToImport)
		self.AnimFrameBuffer = int(AnimFrameBuffer)
		self.UseAsMajoraAnim = int(UseAsMajoraAnim)
	
	def toString(self):
		string = "-%s %s"%("loadOtherSegments",self.loadOtherSegments)
		string += " -%s %s"%("enableMatrices",self.enableMatrices)
		string += " -%s %s"%("ExtractTextures",self.ExtractTextures)
		string += " -%s %s"%("ImportTextures",self.ImportTextures)
		string += " -%s %s"%("ImportBones",self.ImportBones)
		string += " -%s %s"%("enableTexMirror",self.enableTexMirror)
		string += " -%s %s"%("UseAsMajoraAnim",self.UseAsMajoraAnim)
		string += " -%s %s"%("ImportAnim",self.ImportAnim)
		string += " -%s %s"%("ImportAnimAs",self.ImportAnimAs)
		string += " -%s %s"%("ImportAnimsAll",self.ImportAnimsAll)
		string += " -%s %s"%("AnimToImport",self.AnimToImport)
		string += " -%s %s"%("AnimFrameBuffer",self.AnimFrameBuffer)
		
		return string.strip()
	
	def fromString(self, string):
		string = string[1:]
		tokens = string.split('-')
		values = {}
		#print tokens
		for t in tokens:
			o = t.split()
			values[o[0].strip()] = o[1].strip()

		self.ExtractTextures = int(values["ExtractTextures"])
		self.ImportTextures = int(values["ImportTextures"])
		self.ImportBones = int(values["ImportBones"])
		self.loadOtherSegments = int(values["loadOtherSegments"])
		self.enableMatrices = int(values["enableMatrices"])
		self.enableTexMirror = int(values["enableTexMirror"])
		self.ImportAnim = int(values["ImportAnim"])
		self.ImportAnimAs = int(values["ImportAnimAs"])
		self.ImportAnimsAll = int(values["ImportAnimsAll"])
		self.AnimToImport = int(values["AnimToImport"])
		self.AnimFrameBuffer = int(values["AnimFrameBuffer"])
		self.UseAsMajoraAnim = int(values["UseAsMajoraAnim"])

def splitOffset(offset):
	return offset >> 24, offset & 0x00FFFFFF

def translateRotation(rot):
	# axis, angle
	return Matrix.Rotation(rot[3], 4, Vector(rot[:3]))

def validOffset(segment, offset):
	seg, offset = splitOffset(offset)
	if seg > 15:
		return False
	if offset >= len(segment[seg]):
		return False
	return True

def pow2(val):
	i = 1
	while i < val:
		i <<= 1
	return int(i)

def powof(val):
	num, i = 1, 0
	while num < val:
		num <<= 1
		i += 1
	return int(i)

def mulVec(v1, v2):
	return Vector(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z)
	
def up(val,size):
	fmt = format(">%s"%size)
	return unpack_from(fmt,val)[0]
	
def get_mobject(node):
	if (not node):
		return None
	selectionList = OpenMaya.MSelectionList()
	selectionList.add(node)
	if selectionList.getDependNode(0):
		oNode = selectionList.getDependNode(0)
		return oNode
	else:
		return None

def get_mobjname(node):
	if (not node) or (not (type(node) == type(OpenMaya.MObject()))):
		return ""
	oNode = OpenMaya.MFnDependencyNode(node)
	return 	oNode

class Tile:
	def __init__(self):
		self.texFmt, self.texBytes = 0x00, 0
		self.width, self.height = 0, 0
		self.rWidth, self.rHeight = 0, 0
		self.txlSize = 0
		self.lineSize = 0
		self.sizemin = Vector(0, 0)
		self.sizemax = Vector(0, 0)
		self.scale = Vector(1, 1)
		self.ratio = Vector(1, 1)
		self.clip = Vector(0, 0)
		self.mask = Vector(0, 0)
		self.shift = Vector(0, 0)
		self.tshift = Vector(0, 0)
		self.offset = Vector(0, 0)
		self.data = 0x00000000
		self.palette = 0x00000000

	def create(self, segment):
		global fpath, settings
		#print ("Image Create FPath: %s"%fpath)
		texPath = os.path.join(fpath, "textures")
		self.filename = os.path.join(texPath, "%08X.tga" % self.data)
		#print ("Path: %s, Exists: %s"%(texPath, os.path.isdir(texPath)))
		
		#print ("Image Filename: %s"%self.filename)
		if settings.ExtractTextures:
			try:
				os.mkdir(texPath)
			except:
				pass
			
			w = self.rWidth
			if int(self.clip.x) & 1 != 0 and settings.enableTexMirror:
				w <<= 1
			h = self.rHeight
			if int(self.clip.y) & 1 != 0 and settings.enableTexMirror:
				h <<= 1
			file = open(self.filename, 'wb')

			if self.texFmt == 0x40 or self.texFmt == 0x48 or self.texFmt == 0x50:
				file.write(pack("<BBBHHBHHHHBB", 0, 1, 1, 0, 256, 24, 0, 0, w, h, 8, 0))
				self.writePalette(file, segment)
			else:
				file.write(pack("<BBBHHBHHHHBB", 0, 0, 2, 0, 0, 0, 0, 0, w, h, 32, 8))
			if int(self.clip.y) & 1 != 0 and settings.enableTexMirror:
				self.writeImageData(file, segment, True)
			else:
				self.writeImageData(file, segment)
				
			file.close()
			print "Saved Image: %s" % self.filename
		try:
			# Attempt to load the image into Maya
			nfpath, fext = os.path.splitext(self.filename)
			nfpath, name = os.path.split(nfpath)
			finame = MayaSafeName("file_"+name)
			# Load Image File
			#print "Loading Image: %s"%finame
			if not cmds.objExists(finame):
				file_node = cmds.shadingNode('file',name=finame,asTexture=True)
			else:
				file_node = finame
			
			cmds.setAttr(("%s.fileTextureName" % file_node ),self.filename,type="string");			
			# Create the material node
			# NOTE: We're making this a Lambert node!
			#print "create material"
			mname = MayaSafeName("Mat_"+name)
			if not cmds.objExists(mname):
				#create a shader
				shader=cmds.shadingNode("lambert",asShader=True,name=mname)
				# a shading group
				shading_group=cmds.sets(renderable=True,noSurfaceShader=True,empty=True,name=("SG_%s"%name))
				#connect shader to sg surface shader
				cmds.connectAttr('%s.outColor' % shader ,'%s.surfaceShader' % shading_group)
				#connect file texture node to shader's color
				cmds.connectAttr('%s.outColor' %file_node, '%s.color' % shader)
				#cmds.connectAttr('%s.outTransparency' %file_node, '%s.transparency' % shader)
				textureUV = cmds.shadingNode('place2dTexture',name=("place2dTexture_%s"%name),asUtility=True)
				cmds.connectAttr(("%s.outUV" % textureUV), '%s.uvCoord' % file_node)
				
			#print("mname: %s"%mname)	
			return mname
		except:
			print "Unable to load texture: %s" % self.filename
			return None

	def calculateSize(self):
		global settings
		maxTxl, lineShift = 0, 0
		if self.texFmt == 0x00 or self.texFmt == 0x40:
			maxTxl = 4096
			lineShift = 4
		elif self.texFmt == 0x60 or self.texFmt == 0x80:
			maxTxl = 8192
			lineShift = 4
		elif self.texFmt == 0x08 or self.texFmt == 0x48:
			maxTxl = 2048
			lineShift = 3
		elif self.texFmt == 0x68 or self.texFmt == 0x88:
			maxTxl = 4096
			lineShift = 3
		elif self.texFmt == 0x10 or self.texFmt == 0x70:
			maxTxl = 2048
			lineShift = 2
		elif self.texFmt == 0x50 or self.texFmt == 0x90:
			maxTxl = 2048
			lineShift = 0
		elif self.texFmt == 0x18:
			maxTxl = 1024
			lineShift = 2
		lineWidth = self.lineSize << lineShift
		self.lineSize = lineWidth
		tileWidth = self.sizemax.x - self.sizemin.x + 1
		tileHeight = self.sizemax.y - self.sizemin.y + 1
		maskWidth = 1 << int(self.mask.x)
		maskHeight = 1 << int(self.mask.y)
		lineHeight = 0
		if lineWidth > 0:
			lineHeight = min(int(maxTxl / lineWidth), tileHeight)
		if self.mask.x > 0 and (maskWidth * maskHeight) <= maxTxl:
			self.width = maskWidth
		elif (tileWidth * tileHeight) <= maxTxl:
			self.width = tileWidth
		else:
			self.width = lineWidth
		if self.mask.y > 0 and (maskWidth * maskHeight) <= maxTxl:
			self.height = maskHeight
		elif (tileWidth * tileHeight) <= maxTxl:
			self.height = tileHeight
		else:
			self.height = lineHeight
		clampWidth, clampHeight = 0, 0
		if self.clip.x == 1:
			clampWidth = tileWidth
		else:
			clampWidth = self.width
		if self.clip.y == 1:
			clampHeight = tileHeight
		else:
			clampHeight = self.height
		if maskWidth > self.width:
			self.mask.x = powof(self.width)
			maskWidth = 1 << int(self.mask.x)
		if maskHeight > self.height:
			self.mask.y = powof(self.height)
			maskHeight = 1 << int(self.mask.y)
		if int(self.clip.x) & 2 != 0:
			self.rWidth = pow2(clampWidth)
		elif int(self.clip.x) & 1 != 0:
			self.rWidth = pow2(maskWidth)
		else:
			self.rWidth = pow2(self.width)
		if int(self.clip.y) & 2 != 0:
			self.rHeight = pow2(clampHeight)
		elif int(self.clip.y) & 1 != 0:
			self.rHeight = pow2(maskHeight)
		else:
			self.rHeight = pow2(self.height)
		self.shift.x, self.shift.y = 1.0, 1.0
		if self.tshift.x > 10:
			self.shift.x = 1 << int(16 - self.tshift.x)
		elif self.tshift.x > 0:
			self.shift.x /= 1 << int(self.tshift.x)
		if self.tshift.y > 10:
			self.shift.y = 1 << int(16 - self.tshift.y)
		elif self.tshift.y > 0:
			self.shift.y /= 1 << int(self.tshift.y)
		self.ratio.x = (self.scale.x * self.shift.x) / self.rWidth
		self.ratio.x /= 32
		if int(self.clip.x) & 1 != 0 and settings.enableTexMirror:
			self.ratio.x /= 2
		self.offset.x = self.sizemin.x
		self.ratio.y = (self.scale.y * self.shift.y) / self.rHeight
		self.ratio.y /= 32
		if int(self.clip.y) & 1 != 0 and settings.enableTexMirror:
			self.ratio.y /= 2
		self.offset.y = 1.0 + self.sizemin.y

	def writePalette(self, file, segment):
		if self.texFmt == 0x40:
			palSize = 16
		else:
			palSize = 256
		if not validOffset(segment, self.palette + int(palSize * 2) - 1):
			for i in range(256):
				file.write(pack("L", 0))
			return
		seg, offset = splitOffset(self.palette)
		for i in range(256):
			if i < palSize:
				color = unpack_from(">H", segment[seg], offset + (i << 1))[0]
				r = int(8 * ((color >> 11) & 0x1F))
				g = int(8 * ((color >> 6) & 0x1F))
				b = int(8 * ((color >> 1) & 0x1F))
				file.write(pack("BBB", b, g, r))
			else:
				file.write(pack("BBB", 0, 0, 0))

	def writeImageData(self, file, segment, fy=False, df=False):
		global settings
		if fy:
			dir = (0, self.rHeight, 1)
		else:
			dir = (self.rHeight - 1, -1, -1)
		if self.texFmt == 0x40 or self.texFmt == 0x60 or self.texFmt == 0x80 or self.texFmt == 0x90:
			bpp = 0.5
		elif self.texFmt == 0x00 or self.texFmt == 0x08 or self.texFmt == 0x10 or self.texFmt == 0x70:
			bpp = 2
		elif self.texFmt == 0x48 or self.texFmt == 0x50 or self.texFmt == 0x68 or self.texFmt == 0x88:
			bpp = 1
		else:
			bpp = 4
		lineSize = self.rWidth * bpp
		if not validOffset(segment, self.data + int(self.rHeight * lineSize) - 1):
			size = self.rWidth * self.rHeight
			if int(self.clip.x) & 1 != 0 and settings.enableTexMirror:
				size *= 2
			if int(self.clip.y) & 1 != 0 and settings.enableTexMirror:
				size *= 2
			for i in range(size):
				if self.texFmt == 0x40 or self.texFmt == 0x48 or self.texFmt == 0x50:
					file.write(pack("B", 0))
				else:
					file.write(pack(">L", 0x000000FF))
			return
		seg, offset = splitOffset(self.data)
		for i in range(dir[0], dir[1], dir[2]):
			off = offset + int(i * lineSize)
			line = []
			j = 0
			while j < int(self.rWidth * bpp):
				if bpp < 2:
					color = unpack_from("B", segment[seg], off + int(floor(j)))[0]
				elif bpp == 2:
					color = unpack_from(">H", segment[seg], off + j)[0]
				else:
					color = unpack_from(">L", segment[seg], off + j)[0]
				if self.texFmt == 0x40:
					if floor(j) == j:
						a = color >> 4
					else:
						a = color & 0x0F
				elif self.texFmt == 0x48 or self.texFmt == 0x50:
					a = color
				elif self.texFmt == 0x00 or self.texFmt == 0x08 or self.texFmt == 0x10:
					r = int(8 * ((color >> 11) & 0x1F))
					g = int(8 * ((color >> 6) & 0x1F))
					b = int(8 * ((color >> 1) & 0x1F))
					a = int(255 * (color & 0x01))
				elif self.texFmt == 0x80:
					if floor(j) == j:
						r = int(16 * (color >> 4))
					else:
						r = int(16 * (color & 0x0F))
					g = r
					b = g
					a = 0xFF
				elif self.texFmt == 0x88:
					r = color
					g = r
					b = g
					a = 0xFF
				elif self.texFmt == 0x68:
					r = int(16 * (color >> 4))
					g = r
					b = g
					a = int(16 * (color & 0x0F))
				elif self.texFmt == 0x70:
					r = color >> 8
					g = r
					b = g
					a = color & 0xFF
				elif self.texFmt == 0x18:
					r = color >> 24
					g = (color >> 16) & 0xFF
					b = (color >> 8) & 0xFF
					a = color & 0xFF
				else:
					r = 0
					g = 0
					b = 0
					a = 0xFF
				if self.texFmt == 0x40 or self.texFmt == 0x48 or self.texFmt == 0x50:
					line.extend([a])
				else:
					line.extend([(b << 24) | (g << 16) | (r << 8) | a])
				j += bpp
			if self.texFmt == 0x40 or self.texFmt == 0x48 or self.texFmt == 0x50:
				file.write(pack("B" * len(line), *line))
			else:
				file.write(pack(">" + "L" * len(line), *line))
			if int(self.clip.x) & 1 != 0 and settings.enableTexMirror:
				line.reverse()
				if self.texFmt == 0x40 or self.texFmt == 0x48 or self.texFmt == 0x50:
					file.write(pack("B" * len(line), *line))
				else:
					file.write(pack(">" + "L" * len(line), *line))
		if int(self.clip.y) & 1 != 0 and (not df) and settings.enableTexMirror:
			if fy:
				self.writeImageData(file, segment, False, True)
			else:
				self.writeImageData(file, segment, True, True)

class Vertex:
	def __init__(self):
		self.pos = Vector(0, 0, 0)
		self.uv = Vector(0, 0)
		self.normal = Vector(0, 0, 0)
		self.color = [0, 0, 0, 0]
		self.limb = None

	def read(self, segment, offset):
		if not validOffset(segment, offset + 16):
			return
		seg, offset = splitOffset(offset)
		self.pos.x = unpack_from(">h", segment[seg], offset)[0]
		self.pos.z = unpack_from(">h", segment[seg], offset + 2)[0]
		self.pos.y = -unpack_from(">h", segment[seg], offset + 4)[0]
		self.pos /= 48  # UDK Scale
		self.uv.x = float(unpack_from(">h", segment[seg], offset + 8)[0])
		self.uv.y = float(unpack_from(">h", segment[seg], offset + 10)[0])
		self.normal.x = 0.00781250 * unpack_from("b", segment[seg], offset + 12)[0]
		self.normal.z = 0.00781250 * unpack_from("b", segment[seg], offset + 13)[0]
		self.normal.y = 0.00781250 * unpack_from("b", segment[seg], offset + 14)[0]
		self.color[0] = min(0.00392157 * up(segment[seg][offset + 12],"b"), 1.0)
		self.color[1] = min(0.00392157 * up(segment[seg][offset + 13],"b"), 1.0)
		self.color[2] = min(0.00392157 * up(segment[seg][offset + 14],"b"), 1.0)

class UVTarget:
	def __init__(self,iid=0,ix=0,iy=0):
		self.id = iid
		self.x = ix
		self.y = iy

class VertColor:
	def __init__(self,iid=0,ir=0,ig=0,ib=0,ia=1):
		self.id = iid
		self.r = ir
		self.g = ig
		self.b = ib
		self.a = ia
		
class VertNormal:
	def __init__(self,iid=0,ix=0,iy=0,iz=0):
		self.id = iid
		self.x = ix
		self.y = iy
		self.z = iz

class Mesh:
	def __init__(self):
		self.verts = OpenMaya.MFloatPointArray()
		self.uvs, self.colors, self.faces, self.connects, self.UVIds = [], [], [], [], []
		self.vertColors = []
		self.vertNormals = []
		self.uvData = []
		self.materials = []
		self.count = 0
		self.vgroups = {}

	def create(self, hierarchy, offset):
		global fname
		global rootObject
		if len(self.faces) == 0:
			#print "No faces to build."
			return
			
		# Build Mesh Name
		mfName = "mesh%s_%08X"%(MayaSafeName(fname),offset)
		mfName = MayaSafeName(mfName)
		#print "Mesh Name: %s"%mfName
		meshName=cmds.createNode("transform", n=mfName,p=rootObject)
		
		# Define variables
		vertexArray = OpenMaya.MFloatPointArray()
		MeshUVIDs = OpenMaya.MIntArray()
		MeshUVCounts = OpenMaya.MIntArray()

		#print "Collecting Mesh Data..."
		polygonCounts = OpenMaya.MIntArray()
		polygonConnects = OpenMaya.MIntArray()
		for i in range(len(self.faces)):
			polygonCounts.append(3)
			polygonConnects.append(self.faces[i][0])
			polygonConnects.append(self.faces[i][1])
			polygonConnects.append(self.faces[i][2])
			MeshUVCounts.append(3)

		for i in xrange(len(self.verts)):
			pvert = OpenMaya.MFloatPoint(self.verts[i].x,self.verts[i].y,self.verts[i].z)
			vertexArray.append(pvert)

		#print "Creating the Mesh..."
		mesh = OpenMaya.MFnMesh()
		meshNode = mesh.create(vertexArray, polygonCounts, polygonConnects, parent=get_mobject(meshName))
		meshshapename = mesh.fullPathName().rsplit("|")[-1]
		#print "extracted shapename: %s"%meshshapename
		shapename = cmds.rename(meshshapename,"%sShape"%mfName)
		#print "renamed shapename: %s"%shapename
		
		#print "Building UVs..."
		# Assign UVs
		for i in range(len(self.uvData)):
			#print "Index: %s UVID:%s @ X: %f, Y: %f" % (i, self.uvData[i].id,self.uvData[i].x,self.uvData[i].y)
			MeshUVIDs.append(self.UVIds[i])
			mesh.setUV(self.uvData[i].id,self.uvData[i].x,self.uvData[i].y)
		
		#print("len UVData: %s, UVCounts: %s, UVIDs: %s"%(len(self.uvData),len(MeshUVCounts), len(MeshUVIDs)))
		if len(self.uvData)>0 and len(self.uvData) == len(MeshUVIDs):
			mesh.assignUVs(MeshUVCounts, MeshUVIDs)

		cmds.select(shapename,r=True)
		# Assign Materials
		for i in range(len(self.faces)):
			if self.uvs[i * 4 + 3]:
				fMat = self.uvs[i * 4 + 3]
				cmds.hyperShade(assign=fMat)
				#print("Assigning Material: \"%s\" to %s"%(fMat,meshName))
				break

		# Assign Vertex Colors for all the Verts
		colorsetname = mesh.createColorSet("%sColorSet"%MayaSafeName(fname),False)
		for i in range(len(self.vertColors)):
			cc = OpenMaya.MColor()
			cc.r = self.vertColors[i].r
			cc.g = self.vertColors[i].g
			cc.b = self.vertColors[i].b
			#print "Colors R:%f G:%f B:%f A:%f" % (color.r,color.g,color.b,color.a)
			mesh.setVertexColor(cc, self.vertColors[i].id)
			
		# Assign colorsetname to Mesh
		cmds.polyColorSet(currentColorSet=True, colorSet=colorsetname)
		
		# Vertex Normals
		for i in range(len(self.vertNormals)):
			norm = OpenMaya.MVector()
			norm.x = self.vertNormals[i].x
			norm.y = self.vertNormals[i].y
			norm.z = self.vertNormals[i].z
			mesh.setVertexNormal(norm, self.vertNormals[i].id)
		
		# Skin mesh to bones
		if hierarchy and settings.ImportBones == True:
			cmds.select(hierarchy.armature,r=True)
			jointList = cmds.ls(dag=True, type='joint', sl=True)
			clusterName = cmds.skinCluster(meshName, tuple(jointList))
			cmds.select(clusterName,r=True)
			for name, vgroup in self.vgroups.items():
				for v in vgroup:
					vertexNum = "%s.vtx[%s]"%(meshName,v)
					cmds.skinPercent("%s"%tuple(clusterName), vertexNum, transformValue=[(name, 1.0)])
		
		cmds.select(shapename,r=True)
		cmds.polyOptions(colorShadedDisplay=True)
		cmds.polyOptions(colorMaterialChannel="diffuse")
		cmds.polyOptions(materialBlend="multiply")

class Limb:
	def __init__(self):
		self.parent, self.child, self.sibling = -1, -1, -1
		self.pos = Vector(0, 0, 0)
		self.near, self.far = 0x00000000, 0x00000000
		self.poseBone = None
		self.poseLocPath, self.poseRotPath = None, None
		self.poseLoc, self.poseRot = Vector(0, 0, 0), None

	def read(self, segment, offset, actuallimb, BoneCount):
		seg, offset = splitOffset(offset)

		rot_offset = offset & 0xFFFFFF
		rot_offset += (0 * (BoneCount * 6 + 8))

		self.pos.x = unpack_from(">h", segment[seg], offset)[0]
		self.pos.z = unpack_from(">h", segment[seg], offset + 2)[0]
		self.pos.y = -unpack_from(">h", segment[seg], offset + 4)[0]
		self.pos /= 48  # UDK Scale
		self.child = unpack_from("b", segment[seg], offset + 6)[0]
		self.sibling = unpack_from("b", segment[seg], offset + 7)[0]
		self.near = unpack_from(">L", segment[seg], offset + 8)[0]
		self.far = unpack_from(">L", segment[seg], offset + 12)[0]

		self.poseLoc.x = unpack_from(">h", segment[seg], rot_offset)[0]
		self.poseLoc.z = unpack_from(">h", segment[seg], rot_offset + 2)[0]
		self.poseLoc.y = unpack_from(">h", segment[seg], rot_offset + 4)[0]
		# print("	 Limb ", actuallimb, ":", self.poseLoc.x, ",", self.poseLoc.z, ",", self.poseLoc.y)
		
		
class Hierarchy:
	def __init__(self):
		self.name, self.offset = "", 0x00000000
		self.limbCount, self.dlistCount = 0x00, 0x00
		self.limb = []
		self.armature = None
		self.limbOffset = 0

	def read(self, segment, offset):
		#print "Reading Hierarchy..."
		if not validOffset(segment, offset + 9):
			print "Invalid Offset"
			return
		self.name = "sk_%08X" % offset
		self.offset = offset
		seg, offset = splitOffset(offset)
		limbIndex_offset = unpack_from(">L", segment[seg], offset)[0]
		if not validOffset(segment, limbIndex_offset):
			print("	  ERROR:  Limb index table 0x%08X out of range" % limbIndex_offset)
			return
		limbIndex_seg, limbIndex_offset = splitOffset(limbIndex_offset)
		self.limbCount = up(segment[seg][offset + 4],"B")
		self.dlistCount = up(segment[seg][offset + 8],"B")
		for i in range(self.limbCount):
			limb_offset = unpack_from(">L", segment[limbIndex_seg], limbIndex_offset + 4 * i)[0]
			limb = Limb()
			limb.index = i
			self.limb.extend([limb])
			if validOffset(segment, limb_offset + 12):
				limb.read(segment, limb_offset, i, self.limbCount)
			else:
				print("	  ERROR:  Limb 0x%02X offset 0x%08X out of range" % (i, limb_offset))[0]
		self.limb[0].pos = Vector(0, 0, 0)
		self.initLimbs(0x00)
		#print "Finished reading Hierarchy..."

	def create(self):
		global fname
		if settings.ImportBones == False:
			return
		#print "Creating Hierarchy..."
		
		armname = ("Skeleton_%s" % (MayaSafeName(fname)))
		if cmds.objExists(armname):
			self.armature = armname
			cmds.select(armname,r=True)
		else:
			self.armature = cmds.group(em=True, name=armname, w=True)
			cmds.character(self.armature, add=self.charName)
		
		#create joints
		for i in range(self.limbCount):
			pos = self.limb[i].pos
			#print "Limb Parent: %i" % self.limb[i].parent
			limbNameParent = "limb_%02i"%(self.limb[i].parent+self.limbOffset)
			limbName = "limb_%02i"%(i+self.limbOffset)
			if self.limb[i].parent == -1:
				cmds.select(armname,r=True)
			else:
				cmds.select(limbNameParent ,r=True)
			jointname = cmds.joint(n=limbName, p=(pos.x,pos.y,pos.z), roo='xzy')
			cmds.character(jointname, add=self.charName)
			# print("Joint #%02i, final name: %s"%(i, jointname))

	def initLimbs(self, i):
		if (self.limb[i].child > -1 and self.limb[i].child != i):
			self.limb[self.limb[i].child].parent = i
			self.limb[self.limb[i].child].pos += self.limb[i].pos
			self.initLimbs(self.limb[i].child)
		if (self.limb[i].sibling > -1 and self.limb[i].sibling != i):
			self.limb[self.limb[i].sibling].parent = self.limb[i].parent
			self.limb[self.limb[i].sibling].pos += self.limb[self.limb[i].parent].pos
			self.initLimbs(self.limb[i].sibling)

	def getMatrixLimb(self, offset):
		j = 0
		index = (offset & 0x00FFFFFF) / 0x40
		for i in range(self.limbCount):
			if self.limb[i].near != 0:
				if (j == index):
					return self.limb[i]
				j += 1
		return self.limb[0]

class F3DZEX:
	global settings
	global ExternalAnims
	def __init__(self):
		self.segment, self.vbuf, self.tile = [], [], []

		self.animTotal = 0
		self.TimeLine = 0
		self.count = 0
		self.TimeLinePosition = 0
		self.TotalLimbCount = 0
		for i in range(16):
			self.segment.extend([[]])
			self.vbuf.extend([Vertex()])
		for i in range(2):
			self.tile.extend([Tile()])
			self.vbuf.extend([Vertex()])
		for i in range(14 + 32):
			self.vbuf.extend([Vertex()])
		self.curTile = 0
		self.material = []
		self.hierarchy = []
		self.resetCombiner()

	def setSegment(self, seg, path):
		# add if statement here to check that the file exists BEFORE we try to load it.
		try:
			#print "Trying to load segment %s..." % path
			file = open(path,'rb')
			self.segment[seg] = file.read()
			file.close()
			print "Segment %s successfully loaded!"%seg
		except:
			pass
			
	def locateHierarchies(self):
		#print "Number of Segments: %i" % len(self.segment)
		data = self.segment[0x06]
		#print "Length of data in segment 0x06: %i" % len(data)
		for i in range(0, len(data), 4):
			try:
				di = up(data[i],"B")
				di3 = up(data[i + 3],"B")
				di4 = up(data[i + 4],"B")
				#print "Data[%i]: %s VS %s" % (i,0x06,di)
				if di == 0x06 and (di3 & 3) == 0 and di4 != 0:
					#print "Data range found. Checking Offset..."
					offset = unpack_from(">L", data, i)[0] & 0x00FFFFFF
					if offset < len(data):
						offset_end = offset + (di4 << 2)
						if offset_end < len(data):
							#print "Offset Within data range"
							j = offset
							while j < offset_end:
								if up(data[j],"B") != 0x06 or (up(data[j + 3],"B") & 3) != 0 or (unpack_from(">L", data, j)[0] & 0x00FFFFFF) > len(data):
									#print "breaking"
									break
								j += 4
							if (j == i):
								j |= 0x06000000
								print("   hierarchy found at 0x%08X" % j)
								h = Hierarchy()
								h.read(self.segment, j)
								self.hierarchy.extend([h])
			except:
				pass

	def locateAnimations(self):
		data = self.segment[0x06]
		self.animation = []
		self.offsetAnims = []
		for i in range(0, len(data), 4):
			if (
				(data[i] == 0) and (data[i + 1] > 1) and
				(data[i + 2] == 0) and (data[i + 3] == 0) and
				(data[i + 4] == 0x06) and
				(((data[i + 5] << 16) | (data[i + 6] << 8) | data[i + 7]) < len(data)) and
				(data[i + 8] == 0x06) and
				(((data[i + 9] << 16) | (data[i + 10] << 8) | data[i + 11]) < len(data)) and
				(data[i + 14] == 0) and (data[i + 15] == 0)):

				print("		Anims found at %08X" % i, "Frames:", data[i + 1] & 0x00FFFFFF)
				self.animation.extend([i])
				self.offsetAnims.extend([i])
				self.offsetAnims[self.animTotal] = (0x06 << 24) | i
				self.animTotal += 1
		if(self.animTotal > 0):
			print("		Total Anims				   :", self.animTotal)
			
	def locateLinkAnimations(self):
		data = self.segment[0x04]
		self.animation = []
		self.offsetAnims = []
		self.animFrames = []
		self.animTotal = -1
		if (len(self.segment[0x04]) > 0):
			rangeStart = 0x2310
			rangeEnd = 0x34F8
			if (settings.UseAsMajoraAnim == True):
				rangeStart = 0xD000
				rangeEnd = 0xE4F8
			
			for i in range(rangeStart, rangeEnd, 8):
				self.animTotal += 1
				self.animation.extend([self.animTotal])
				self.animFrames.extend([self.animTotal])
				self.offsetAnims.extend([self.animTotal])
				self.offsetAnims[self.animTotal] = unpack_from(">L", data, i + 4)[0]
				self.animFrames[self.animTotal] = unpack_from(">h", data, i)[0]
				# print("- Animation #", self.animTotal+1, "offset: %07X" % self.offsetAnims[self.animTotal], "frames:", self.animFrames[self.animTotal])

			print("	   Link has come to town!!!!")
		if ((len(self.segment[0x07]) > 0) and (self.animTotal > 0)):
			self.buildLinkAnimations(self.hierarchy[0])

	def importMap(self):
		data = self.segment[0x03]
		for i in range(0, len(data), 8):
			#print("for %i, Data[i]: 0x%02X = 0x0A, Data[i+4]: 0x%02X = 0x03"%(i, ord(data[i]), ord(data[i + 4])))
			if (ord(data[i]) == 0x0A and ord(data[i + 4]) == 0x03):
				mho = (ord(data[i + 5]) << 16) | (ord(data[i + 6]) << 8) | ord(data[i + 7])
				if (mho < len(data)):
					type = up(data[mho],"B")
					count = up(data[mho+1],"B")
					seg = ord(data[mho+4])
					start = (ord(data[mho + 5]) << 16) | (ord(data[mho + 6]) << 8) | ord(data[mho + 7])
					end = (ord(data[mho + 9]) << 16) | (ord(data[mho + 10]) << 8) | ord(data[mho + 11])
					if (ord(data[mho + 4]) == 0x03 and start < end and end < len(data)):
						if (type == 0):
							for j in range(start, end, 4):
								self.buildDisplayList(None, [None], unpack_from(">L", data, j)[0])
						elif (type == 2):
							for j in range(start, end, 16):
								near = (ord(data[j + 8]) << 24) | (ord(data[j + 9]) << 16) | (ord(data[j + 10]) << 8) | ord(data[j + 11])
								far = (ord(data[j + 12]) << 24) | (ord(data[j + 13]) << 16) | (ord(data[j + 14]) << 8) | ord(data[j + 15])
								if (near != 0):
									self.buildDisplayList(None, [None], near)
								elif (far != 0):
									self.buildDisplayList(None, [None], far)
				return
			elif (ord(data[i]) == 0x14):
				break
		print("ERROR:  Map header not found")

	def importObj(self):
		global fname
		print("Importing Object...\nLocating hierarchies...")
		self.locateHierarchies()
		print "Finished Looking for Hierarchies. Processing..."
		print "Number of hierarchies found: %i" % len(self.hierarchy)
		cmds.select(cl=True)
		self.charName = cmds.character(name="Character_%s"%MayaSafeName(fname))
		for hierarchy in reversed(self.hierarchy):
			print("\nBuilding hierarchy '%s'..." % hierarchy.name)
			hierarchy.parentGroup = self.rootname
			hierarchy.charName = self.charName
			hierarchy.limbOffset = self.TotalLimbCount
			self.TotalLimbCount = self.TotalLimbCount + hierarchy.limbCount
			#Create Hierarchy
			hierarchy.create()
			for i in range(hierarchy.limbCount):
				limb = hierarchy.limb[i]
				if limb.near != 0:
					if validOffset(self.segment, limb.near):
						#print("   0x%02X : building display lists..." % i)
						self.resetCombiner()
						self.buildDisplayList(hierarchy, limb, limb.near)
					else:
						print("   0x%02X : out of range" % i)
				else:
					print("   0x%02X : n/a" % i)
					
		self.combineMeshes()
		
		if len(self.hierarchy) > 0:
			print "Hierarchy Found. Loading Animations..."
			if (settings.AnimToImport > 0):
				if(ExternalAnims and len(self.segment[0x0F]) > 0):
					self.locateExternAnimations()
				else:
					self.locateAnimations()
				if len(self.animation) > 0:
					self.buildAnimations(self.hierarchy[0], 0)
				else:
					self.locateLinkAnimations()
			else:
				print("   Load anims OFF.")

	def combineMeshes(self):
		list = cmds.ls(g=True)
		#cmds.polyUniteSkinned(list,n="%s_FinalMesh"%self.fname,ch=False,muv=1)	#Maya 2015+
		#cmds.polyUnite(list,n="%s_FinalMesh"%self.fname,ch=False)
	
	
	def resetCombiner(self):
		self.primColor = Vector(1.0, 1.0, 1.0)
		self.envColor = Vector(1.0, 1.0, 1.0)
		self.vertexColor = Vector(1.0, 1.0, 1.0)
		self.shadeColor = Vector(1.0, 1.0, 1.0)
		
	def getCombinerColor(self):
		cc = Vector(1.0, 1.0, 1.0)
		cc = mulVec(cc, self.primColor)
		cc = mulVec(cc, self.envColor)
		cc = mulVec(cc, self.shadeColor)
		return cc		

	def buildDisplayList(self, hierarchy, limb, offset):
		data = self.segment[offset >> 24]
		mesh = Mesh()
		has_tex = False
		material = None
		if hierarchy:
			matrix = [limb]
		else:
			matrix = [None]
		for i in range(offset & 0x00FFFFFF, len(data), 8):
			w0 = unpack_from(">L", data, i)[0]
			w1 = unpack_from(">L", data, i + 4)[0]
			if up(data[i],"B") == 0x01:
				mesh.count = (up(data[i + 1],"B") << 4) | (up(data[i + 2],"B") >> 4)
				index = (up(data[i + 3],"B") >> 1) - mesh.count
				offset = unpack_from(">L", data, i + 4)[0]
				if validOffset(self.segment, offset + int(16 * mesh.count) - 1):
					for j in range(mesh.count):
						self.vbuf[index + j].read(self.segment, offset + 16 * j)
						if hierarchy:
							self.vbuf[index + j].limb = matrix[len(matrix) - 1]
							if self.vbuf[index + j].limb:
								self.vbuf[index + j].pos += self.vbuf[index + j].limb.pos
			elif up(data[i],"B") == 0x02:
				index = ((up(data[i + 2],"B") & 0x0F) << 3) | (up(data[i + 3],"B") >> 1)
				if up(data[i + 1],"B") == 0x10:
					self.vbuf[index].normal.x = 0.00781250 * unpack_from("b", data, i + 4)[0]
					self.vbuf[index].normal.z = 0.00781250 * unpack_from("b", data, i + 5)[0]
					self.vbuf[index].normal.y = 0.00781250 * unpack_from("b", data, i + 6)[0]
					self.vbuf[index].color = 0.00392157 * unpack_from("BBBB", data, i + 4)[0]
				elif up(data[i + 1],"B") == 0x14:
					self.vbuf[index].uv.x = float(unpack_from(">h", data, i + 4)[0])
					self.vbuf[index].uv.y = float(unpack_from(">h", data, i + 6)[0])
			elif up(data[i],"B") == 0x05 or up(data[i],"B") == 0x06:
				# Vertex Info
				if has_tex:
					material = None
					for j in range(len(self.material)):
						if self.material[j] == "Mat_%08X" % self.tile[0].data:
							material = self.material[j]
							break
					if material is None:
						material = self.tile[0].create(self.segment)
						if material:
							self.material.extend([material])
					has_tex = False
				v1, v2 = None, None
				vi1, vi2 = -1, -1
				if not settings.ImportTextures:
					material = None
				count = 0
				#print "vert len: %i" % len(mesh.verts)
				try:
					#print "J len: %i\n" % (up(data[i],"B") - 4) * 4
					for j in range(1, (up(data[i],"B") - 4) * 4):
						#print "j: %i" %j
						if j != 4:
							#print "Getting UVs"
							index = up(data[i + j],"B") >> 1
							v3 = self.vbuf[index]
							vi3 = -1
							pvert = OpenMaya.MFloatPoint(v3.pos.x, v3.pos.y, v3.pos.z, 0)
							#for k in range(len(mesh.verts)):
							#	if mesh.verts[k] == pvert:
							#		vi3 = k
							#		break
							if vi3 == -1:
								mesh.verts.append(pvert)
								vi3 = len(mesh.verts) - 1
								count += 1
							if j == 1 or j == 5:
								v1 = v3
								vi1 = vi3
							elif j == 2 or j == 6:
								v2 = v3
								vi2 = vi3
							elif j == 3 or j == 7:
								#print "Might get faces..."
								
								# Normal Vector
								mesh.vertNormals.append(VertNormal(vi3, v3.normal.x, v3.normal.y, v3.normal.z))
								mesh.vertNormals.append(VertNormal(vi2, v2.normal.x, v2.normal.y, v2.normal.z))
								mesh.vertNormals.append(VertNormal(vi1, v1.normal.x, v1.normal.y, v1.normal.z))
								
								# Vertext Colors
								sc = (((v3.normal.x + v3.normal.y + v3.normal.z) / 3) + 1.0) / 2
								self.shadeColor = Vector(sc, sc, sc)
								c = self.getCombinerColor()
								mesh.vertColors.append(VertColor(vi3,c[0], c[1], c[2]))
								sc = (((v2.normal.x + v2.normal.y + v2.normal.z) / 3) + 1.0) / 2
								self.shadeColor = Vector(sc, sc, sc)
								c = self.getCombinerColor()
								mesh.vertColors.append(VertColor(vi2,c[0], c[1], c[2]))
								sc = (((v1.normal.x + v1.normal.y + v1.normal.z) / 3) + 1.0) / 2
								self.shadeColor = Vector(sc, sc, sc)
								c = self.getCombinerColor()
								mesh.vertColors.append(VertColor(vi1,c[0], c[1], c[2]))

								#print "I3: %i, I2: %i, I1: %i"%(vi3, vi2, vi1)
								mesh.materials.extend(material)
								mesh.uvs.extend([(self.tile[0].offset.x + v3.uv.x * self.tile[0].ratio.x, self.tile[0].offset.y - v3.uv.y * self.tile[0].ratio.y),
												(self.tile[0].offset.x + v2.uv.x * self.tile[0].ratio.x, self.tile[0].offset.y - v2.uv.y * self.tile[0].ratio.y),
												(self.tile[0].offset.x + v1.uv.x * self.tile[0].ratio.x, self.tile[0].offset.y - v1.uv.y * self.tile[0].ratio.y),
												material])

								#print "UVID Append: %i" % index
								mesh.UVIds.append(vi1)
								mesh.UVIds.append(vi2)
								mesh.UVIds.append(vi3)
								mesh.uvData.append(UVTarget(vi1, self.tile[0].offset.x + v1.uv.x * self.tile[0].ratio.x ,self.tile[0].offset.y - v1.uv.y * self.tile[0].ratio.y))
								mesh.uvData.append(UVTarget(vi2, self.tile[0].offset.x + v2.uv.x * self.tile[0].ratio.x ,self.tile[0].offset.y - v2.uv.y * self.tile[0].ratio.y))
								mesh.uvData.append(UVTarget(vi3, self.tile[0].offset.x + v3.uv.x * self.tile[0].ratio.x ,self.tile[0].offset.y - v3.uv.y * self.tile[0].ratio.y))

								#print "getting hier"
								if hierarchy:
									if v3.limb:
										boneIndex = "limb_%02i" % (v3.limb.index+hierarchy.limbOffset)
										if not (boneIndex in mesh.vgroups):
											mesh.vgroups[boneIndex] = []
										mesh.vgroups[boneIndex].extend([vi3])
									if v2.limb:
										boneIndex = "limb_%02i" % (v2.limb.index+hierarchy.limbOffset)
										if not (boneIndex in mesh.vgroups):
											mesh.vgroups[boneIndex] = []
										mesh.vgroups[boneIndex].extend([vi2])
									if v1.limb:
										boneIndex = "limb_%02i" % ( v1.limb.index+hierarchy.limbOffset)
										if not (boneIndex in mesh.vgroups):
											mesh.vgroups[boneIndex] = []
										mesh.vgroups[boneIndex].extend([vi1])
								mesh.faces.extend([(vi1, vi2, vi3)])
								#print "Faces added."
							#print "ping5"
				except:
					print "vert exception"
					# Remove all verts on failure.
					mesh.verts.clear()
			elif up(data[i],"B") == 0xD7:
				pass
			elif up(data[i],"B") == 0xD8 and settings.enableMatrices:
				if hierarchy and len(matrix) > 1:
					matrix.pop()
			elif up(data[i],"B") == 0xDA and settings.enableMatrices:
				if hierarchy and up(data[i + 4],"B") == 0x0D:
					if (up(data[i + 3],"B") & 0x04) == 0:
						matrixLimb = hierarchy.getMatrixLimb(unpack_from(">L", data, i + 4)[0])
						if (up(data[i + 3],"B") & 0x02) == 0:
							newMatrixLimb = Limb()
							newMatrixLimb.index = matrixLimb.index
							newMatrixLimb.pos = (Vector(matrixLimb.pos.x, matrixLimb.pos.y, matrixLimb.pos.z) + matrix[len(matrix) - 1].pos) / 2
							matrixLimb = newMatrixLimb
						if (up(data[i + 3],"B") & 0x01) == 0:
							matrix.extend([matrixLimb])
						else:
							matrix[len(matrix) - 1] = matrixLimb
					else:
						matrix.extend([matrix[len(matrix) - 1]])
				elif hierarchy:
					print("unknown limb %08X %08X" % (w0, w1))
			elif up(data[i],"B") == 0xDE:
				mesh.create(hierarchy, offset)
				mesh.__init__()
				offset = (offset >> 24) | i + 8
				if validOffset(self.segment, w1):
					self.buildDisplayList(hierarchy, limb, w1)
				if up(data[i + 1],"B") != 0x00:
					return
			elif up(data[i],"B") == 0xDF:
				mesh.create(hierarchy, offset)
				return
			elif up(data[i],"B") == 0xE7:
				mesh.create(hierarchy, offset)
				mesh.__init__()
				offset = (offset >> 24) | i
			elif up(data[i],"B") == 0xF0:
				self.palSize = ((w1 & 0x00FFF000) >> 13) + 1
			elif up(data[i],"B") == 0xF2:
				self.tile[self.curTile].sizemin.x = (w0 & 0x00FFF000) >> 14
				self.tile[self.curTile].sizemin.y = (w0 & 0x00000FFF) >> 2
				self.tile[self.curTile].sizemax.x = (w1 & 0x00FFF000) >> 14
				self.tile[self.curTile].sizemax.y = (w1 & 0x00000FFF) >> 2
				self.tile[self.curTile].width = (self.tile[self.curTile].sizemax.x - self.tile[self.curTile].sizemin.x) + 1
				self.tile[self.curTile].height = (self.tile[self.curTile].sizemax.y - self.tile[self.curTile].sizemin.y) + 1
				self.tile[self.curTile].texBytes = int(self.tile[self.curTile].width * self.tile[self.curTile].height) << 1
				if (self.tile[self.curTile].texBytes >> 16) == 0xFFFF:
					self.tile[self.curTile].texBytes = self.tile[self.curTile].size << 16 >> 15
				self.tile[self.curTile].calculateSize()
			elif up(data[i],"B") == 0xF4 or up(data[i],"B") == 0xE4 or up(data[i],"B") == 0xFE or up(data[i],"B") == 0xFF:
				print("%08X : %08X" % (w0, w1))
			elif up(data[i],"B") == 0xF5:
				self.tile[self.curTile].texFmt = (w0 >> 16) & 0xFF
				self.tile[self.curTile].txlSize = (w0 >> 19) & 0x03
				self.tile[self.curTile].lineSize = (w0 >> 9) & 0x1F
				self.tile[self.curTile].clip.x = (w1 >> 8) & 0x03
				self.tile[self.curTile].clip.y = (w1 >> 18) & 0x03
				self.tile[self.curTile].mask.x = (w1 >> 4) & 0x0F
				self.tile[self.curTile].mask.y = (w1 >> 14) & 0x0F
				self.tile[self.curTile].tshift.x = w1 & 0x0F
				self.tile[self.curTile].tshift.y = (w1 >> 10) & 0x0F
			elif up(data[i],"B") == 0xFA:
				self.primColor = Vector(min(0.003922 * ((w1 >> 24) & 0xFF), 1.0), min(0.003922 * ((w1 >> 16) & 0xFF), 1.0), min(0.003922 * ((w1 >> 8) & 0xFF), 1.0))
			elif up(data[i],"B") == 0xFB:
				self.envColor = Vector(min(0.003922 * ((w1 >> 24) & 0xFF), 1.0), min(0.003922 * ((w1 >> 16) & 0xFF), 1.0), min(0.003922 * ((w1 >> 8) & 0xFF), 1.0))
				#if invertEnvColor:
				#	self.envColor = Vector(1.0, 1.0, 1.0) - self.envColor
			elif up(data[i],"B") == 0xFD:
				try:
					if up(data[i - 8],"B") == 0xF2:
						self.curTile = 1
					else:
						self.curTile = 0
				except:
					pass
				try:
					if up(data[i + 8],"B") == 0xE8:
						self.tile[0].palette = w1
					else:
						self.tile[self.curTile].data = w1
				except:
					pass
				has_tex = True

	def buildAnimClip(self, hierarchy, currentanim, nextStartTime):
		global settings
		global fname
		animEndTime = self.animFrames[currentanim]-1
		if (settings.ImportAnimAs == 1):
			self.buildLinkAnim(hierarchy, 0, currentanim, 0)
			animName = MayaSafeName("anim_%s_%03d"%(fname,currentanim+1))
			clipname = cmds.clip(self.charName, startTime=0, endTime=animEndTime, name=animName, scheduleClip=False)
			clipSched = cmds.character(self.charName, query=True, sc=True)
			cmds.select(clipSched, r=True)
			instanceName = "%sSource"%animName
			names = mel.eval('clipSchedule -instance {0} -name {1} -s {2}'.format(instanceName, animName, nextStartTime))
			cmds.rename("animClip1", animName)
		elif (settings.ImportAnimAs == 0):
			self.buildLinkAnim(hierarchy, 0, currentanim, nextStartTime)
		nextStartTime = nextStartTime + animEndTime
		return nextStartTime
				
	def buildLinkAnimations(self, hierarchy):
		global settings
		global fname
		numAnims = self.animTotal
		if (settings.ImportBones == 0) or (settings.ImportAnim == 0):
			return
		
		nextStartTime = 0
		if (settings.ImportAnimsAll == 1):
			animStart = 1
			animEnd = numAnims # debuggers can limit this for testing.
			for currentanim in range(animStart, animEnd+1):
				canim = currentanim-1
				nextStartTime = self.buildAnimClip(hierarchy, canim, nextStartTime)
				if (settings.ImportAnimAs == 0):
					nextStartTime = nextStartTime+settings.AnimFrameBuffer
		else:
			if (settings.AnimToImport > 0 and settings.AnimToImport <= numAnims):
				currentanim = settings.AnimToImport - 1
				nextStartTime = self.buildAnimClip(hierarchy, currentanim, 0)
			else:
				currentanim = 0
				nextStartTime = 30
			
		if (settings.ImportAnimsAll == 1) and (settings.ImportAnimAs == 0):
			nextStartTime = nextStartTime - settings.AnimFrameBuffer
		cmds.playbackOptions(min=0, ast=0, max=nextStartTime, aet=nextStartTime)
		
	def buildLinkAnim(self, hierarchy, newframe, animNum, frameoffset = 0):
		segment = []
		rot_indx = 0
		rot_indy = 0
		rot_indz = 0
		data = self.segment[0x06]
		segment = self.segment
		n_anims = self.animTotal
		seg, offset = splitOffset(hierarchy.offset)
		BoneCount = hierarchy.limbCount
		RX, RY, RZ = 0, 0, 0
		frameCurrent = newframe
		
		if (animNum < 0 or animNum >= n_anims):
			print("Specified Animation is outside the normal range of 0-%s"%n_anims)
			animNum = 0
			
		if (frameCurrent == 0):
			print("Getting animation #%03i with %i frames..."%(animNum, self.animFrames[animNum]))

		#print("currentanim:", animNum, "frameCurrent:", frameCurrent + 1)
		AnimationOffset = self.offsetAnims[animNum]
		TAnimationOffset = self.offsetAnims[animNum]
		AniSeg = AnimationOffset >> 24
		AnimationOffset &= 0xFFFFFF
		rot_offset = AnimationOffset
		rot_offset += (frameCurrent * (BoneCount * 6 + 8))
		frameTotal = self.animFrames[animNum]
		rot_offset += BoneCount * 6

		Trot_offset = TAnimationOffset & 0xFFFFFF
		Trot_offset += (frameCurrent * (BoneCount * 6 + 8))
		#print("aniseg: %s, segment: %s, Trot Offset: %s"%(AniSeg, len(segment[AniSeg]), Trot_offset))
		if (Trot_offset >= len(segment[AniSeg])):
			return;
		
		TRX = unpack_from(">h", segment[AniSeg], Trot_offset)[0]
		Trot_offset += 2
		TRZ = unpack_from(">h", segment[AniSeg], Trot_offset)[0]
		Trot_offset += 2
		TRY = unpack_from(">h", segment[AniSeg], Trot_offset)[0]
		Trot_offset += 2
		BoneListListOffset = unpack_from(">L", segment[seg], offset)[0]
		BoneListListOffset &= 0xFFFFFF

		BoneOffset = unpack_from(">L", segment[seg], BoneListListOffset + (0 << 2))[0]
		S_Seg = (BoneOffset >> 24) & 0xFF
		BoneOffset &= 0xFFFFFF
		TRX += unpack_from(">h", segment[S_Seg], BoneOffset)[0]
		TRZ += unpack_from(">h", segment[S_Seg], BoneOffset + 2)[0]
		TRY += unpack_from(">h", segment[S_Seg], BoneOffset + 4)[0]
		newLocx = TRX / 79
		newLocz = -25.5
		newLocz += TRZ / 79
		newLocy = TRY / 79
		cmds.currentTime(newframe+frameoffset, update=False)
		
		LimbID = "limb_%02i"%(0)
		if (frameCurrent < frameTotal):
			cmds.currentTime(frameCurrent+frameoffset, update=False)

			cmds.select(LimbID, r=True)
			cmds.setKeyframe(LimbID, v=-newLocx, at='translateX')
			cmds.setKeyframe(LimbID, v=-newLocy, at='translateY')
			cmds.setKeyframe(LimbID, v=newLocz, at='translateZ')
			
			rot_offset = AnimationOffset
			rot_offset += (frameCurrent * (BoneCount * 6 + 8))
			rot_offset += 6
			for i in range(BoneCount):
				RX = unpack_from(">h", segment[AniSeg], rot_offset)[0]
				rot_offset += 2
				RY = unpack_from(">h", segment[AniSeg], rot_offset)[0]
				rot_offset += 2
				RZ = unpack_from(">h", segment[AniSeg], rot_offset)[0]
				rot_offset += 2

				RX /= (182.04444444444444444444)
				RY /= (182.04444444444444444444)
				RZ /= (182.04444444444444444444)

				RXX = (RX)
				RYY = (-RZ)
				RZZ = (RY)
			
				#print("limb:", i,"RX", int(RXX), "RZ", int(RZZ), "RY", int(RYY), "anim:", animNum+1, "frame:", frameCurrent+1)
				if (i > -1):
					LimbID = "limb_%02i" % (i)
					cmds.select(LimbID, r=True)
					
					#add animated rotations
					cmds.setKeyframe(LimbID, v=RXX, at='rotateX')
					cmds.setKeyframe(LimbID, v=RYY, at='rotateY')
					cmds.setKeyframe(LimbID, v=RZZ, at='rotateZ')
			
			frameCurrent += 1
			self.buildLinkAnim(hierarchy, frameCurrent, animNum, frameoffset)
		else:
			cmds.currentTime(0, update=False)
			pass

#main Command
class ImportZelda64(OpenMayaMPx.MPxFileTranslator):
	global settings
	global fpath
	global fname
	global AnimtoPlay
	def __init__(self):
		OpenMayaMPx.MPxFileTranslator.__init__(self)
	
	#Can not be Exported
	def canBeOpened(self):
		return False
		
	#Can Import
	def haveReadMethod(self):
		return True
		
	#Filters and Extensions
	def defaultExtension(self):
		return "zobj"	
	def filter(self):
		return "*.zobj *.zmap"

	def readFile(self,file,options):
		global fname
		global fpath
		global rootObject
		#print "Supplied Options: %s"%options
		options = re.sub('[;]','',options)
		settings.fromString(options)
		AnimtoPlay = settings.AnimToImport

		self.filepath = os.path.normpath(file.fullName())
		filePathName, fext = os.path.splitext(self.filepath)
		filePath, fname = os.path.split(filePathName)
		fname = MayaSafeName(fname)
		fpath = os.path.normpath(filePath)
		print "fpath: %s, fname: %s, fext: %s" % (fpath, fname, fext)
		if cmds.upAxis( q=True, axis=True ) != "z":
			cmds.upAxis(ax='z',rv=True)
		
		sys.stdout.write("\nAttempting to load %s...\n" % self.filepath)
		time_start = time.time()
		
		f3dzex = F3DZEX()
		
		# Build a parent Group using the filename
		if cmds.objExists(fname):
			cmds.delete(fname,hi="below")
			
		rootGroupName = MayaSafeName("root_%s" % fname)
		if not cmds.objExists(rootGroupName):
			rootObject = cmds.group(em=True, name=rootGroupName,w=True)
		else:
			rootObject = rootGroupName
		f3dzex.rootname = rootObject
		if settings.loadOtherSegments == True or settings.ImportAnim == True:
			for i in range(16):
				# f3dzex.setSegment(i, fpath + "/segment_%02X.zdata" % i)
				f3dzex.setSegment(i, os.path.join(fpath, "segment_%02X.zdata" % i))
		#Check to see if the file is a zmap or a zobj
		if(fext.lower() == ".zmap"):
			print("loading map...")
			f3dzex.setSegment(0x03, self.filepath)
			f3dzex.importMap()
		elif(fext.lower() == ".zobj"):
			print("loading model...")
			f3dzex.setSegment(0x06, self.filepath)
			f3dzex.importObj()
		else:
			return 1
		sys.stdout.write("Finished Reading File:  Elapsed time %.4f sec\n" % (time.time() - time_start))
		cmds.select(cl=True)
		cmds.currentUnit(time='ntsc')
		
	#Read the File
	def reader(self,file,options,mode):
		return self.readFile(file,options)

# Creator
def translatorCreator():
	print ("// %s, v%s"%(PluginName, Version))
	return OpenMayaMPx.asMPxPtr(ImportZelda64())

#initialize the script plug-in
def initializePlugin(mobject):
	global settings
	mplugin = OpenMayaMPx.MFnPlugin(mobject, "Kjasi", Version, "Any")
	settings = Zelda64Settings()
	try:
		mplugin.registerFileTranslator(PluginName, None, translatorCreator, "Zelda64Options", settings.toString())
	except:
		sys.stderr.write("Failed to register command: %s\n" % "Zelda64Reader")
		raise
		
#uninitialize the script plug-in
def uninitializePlugin(mobject):
	mplugin = OpenMayaMPx.MFnPlugin(mobject)
	try:
		mplugin.deregisterFileTranslator(PluginName)
	except:
		sys.stderr.write( "Failed to unregister command: %s\n" % "Zelda64Reader" )