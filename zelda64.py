import os
import struct
from struct import unpack_from

def up(val,size,offset=0):
	fmt = format(">%s"%size)
	return unpack_from(fmt,val,offset)[0]

def splitOffset(offset):
	return offset >> 24, offset & 0x00FFFFFF
	
def validOffset(segment, offset):
	seg, offset = splitOffset(offset)
	if seg > 15:
		return False
	if offset >= len(segment[seg]):
		return False
	return True

class Vertex:
	def __init__(self):
		self.pos = Vector(0, 0, 0)
		self.uv = Vector(0, 0)
		self.normal = Vector(0, 0, 0)
		self.color = [0, 0, 0, 0]

	def read(self, segment, offset):
		if not validOffset(segment, offset + 16):
			return
		seg, offset = splitOffset(offset)
		self.pos.x = unpack_from(">h", segment[seg], offset)[0]
		self.pos.z = unpack_from(">h", segment[seg], offset + 2)[0]
		self.pos.y = -unpack_from(">h", segment[seg], offset + 4)[0]
		self.pos /= 48  # UDK Scale
		self.uv.x = float(unpack_from(">h", segment[seg], offset + 8)[0])
		self.uv.y = float(unpack_from(">h", segment[seg], offset + 10)[0])
		self.normal.x = 0.00781250 * unpack_from("b", segment[seg], offset + 12)[0]
		self.normal.z = 0.00781250 * unpack_from("b", segment[seg], offset + 13)[0]
		self.normal.y = 0.00781250 * unpack_from("b", segment[seg], offset + 14)[0]
		self.color[0] = min(0.00392157 * up(segment[seg][offset + 12],"b"), 1.0)
		self.color[1] = min(0.00392157 * up(segment[seg][offset + 13],"b"), 1.0)
		self.color[2] = min(0.00392157 * up(segment[seg][offset + 14],"b"), 1.0)

class Limb:
	def __init__(self):
		self.parent, self.child, self.sibling = -1, -1, -1
		self.pos = Vector(0, 0, 0)
		self.near, self.far = 0x00000000, 0x00000000
		self.poseBone = None
		self.poseLocPath, self.poseRotPath = None, None
		self.poseLoc, self.poseRot = Vector(0, 0, 0), None

	def read(self, segment, offset, actuallimb, BoneCount):
		seg, offset = splitOffset(offset)

		rot_offset = offset & 0xFFFFFF
		rot_offset += (0 * (BoneCount * 6 + 8))

		self.pos.x = unpack_from(">h", segment[seg], offset)[0]
		self.pos.z = unpack_from(">h", segment[seg], offset + 2)[0]
		self.pos.y = -unpack_from(">h", segment[seg], offset + 4)[0]
		self.pos /= 48  # UDK Scale
		self.child = unpack_from("b", segment[seg], offset + 6)[0]
		self.sibling = unpack_from("b", segment[seg], offset + 7)[0]
		self.near = unpack_from(">L", segment[seg], offset + 8)[0]
		self.far = unpack_from(">L", segment[seg], offset + 12)[0]

		self.poseLoc.x = unpack_from(">h", segment[seg], rot_offset)[0]
		self.poseLoc.z = unpack_from(">h", segment[seg], rot_offset + 2)[0]
		self.poseLoc.y = unpack_from(">h", segment[seg], rot_offset + 4)[0]
		# print("	 Limb ", actuallimb, ":", self.poseLoc.x, ",", self.poseLoc.z, ",", self.poseLoc.y)
		
		
class ZeldaModel:
	def __init__(self):
		self.File = []
		self.Verticies = []
		
		for i in range(16):
			self.Verticies.extend([Vertex()])
		for i in range(2):
			self.Verticies.extend([Vertex()])
		for i in range(14 + 32):
			self.Verticies.extend([Vertex()])

	def loadFile(self, id, file):
		try:
			ofile = open(file,'rb')
			self.File[id] = ofile.read()
			ofile.close()
			print "Sucessfully loaded the file."
		except:
			print "Failed to load segment." % file
			pass
		
	def readMeshData(self):
		has_tex = False
		material = None
		
		data = self.File
		
		# Read out the data
		for i in range(offset & 0x00FFFFFF, len(data), 8):
			w0 = up(data, ">L", i)[0]
			w1 = up(data, ">L", i+4)[0]
			
			if up(data[i],"B") == 0x01:
				count = (up(data[i+1],"B") >> 4) | (up(data[i+2],"B") >> 4)
				index = (up(data[i+3],"B") >> 1) - count
				offset = up(data,">L",i + 4)[0]
				if validOffset(self.File, offset + int(16*count)-1):
					#Read Verticies
					for j in range(count):
						self.Verticies[index+j].read(self.File,offset+16*j)
						#if hierarchy
				
			elif up(data[i],"B") == 0x02:
				# get Normal & UV Data
				index = ((up(data[i + 2],"B") & 0x0F) << 3) | (up(data[i + 3],"B") >> 1)
				if up(data[i + 1],"B") == 0x10:
					self.Verticies[index].normal.x = 0.00781250 * unpack_from("b", data, i + 4)[0]
					self.Verticies[index].normal.z = 0.00781250 * unpack_from("b", data, i + 5)[0]
					self.Verticies[index].normal.y = 0.00781250 * unpack_from("b", data, i + 6)[0]
					self.Verticies[index].color = 0.00392157 * unpack_from("BBBB", data, i + 4)[0]
				elif up(data[i + 1],"B") == 0x14:
					print "Getting UV Data"
					self.Verticies[index].uv.x = float(unpack_from(">h", data, i + 4)[0])
					self.Verticies[index].uv.y = float(unpack_from(">h", data, i + 6)[0])
					print "UV Data gathered"
				elif up(data[i],"B") == 0x05 or up(data[i],"B") == 0x06:
					# Vertex Info
				elif up(data[i],"B") == 0xD7:
					pass

