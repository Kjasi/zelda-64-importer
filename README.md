# Zelda 64 Model Import Scripts #

This is a collection of scripts aimed at being able to open models and maps from the Nintendo 64 Legend of Zelda games. (Ocarina of Time and Majora's Mask.) Character models should import fully rigged and with animation.

## Current Scripts ##

* Original Blender Script by [SoulofDeity with animation support by RodLima](https://code.google.com/p/sods-blender-plugins/)
* Maya version by Kjasi
* C++ File Reader program

## How do I Install the scripts? ##

* [Maya Installation](https://bitbucket.org/Kjasi/zelda-64-importer/wiki/install/maya)
* [Blender Installation](https://bitbucket.org/Kjasi/zelda-64-importer/wiki/install/blender)

## How do I use the scripts? ##

### Maya ###
* Go to `File->Import...` and change the `Files of Type` to `Zelda 64 Reader`
* [Change your Options](https://bitbucket.org/Kjasi/zelda-64-importer/wiki/Options Maya) (if needed) on the right-hand side
* Select your Model or Map, and hit import.

### Blender ###
* Go to File->Import->Zelda 64...
* [Change your options](https://bitbucket.org/Kjasi/zelda-64-importer/wiki/Options Blender) (if needed) in the bottom left
* Select your Model or Map, and hit import.

### C++ File Reader ###

This program isn't usable, except as a C++ example of how to read a ZObject and ZMap. When finished, the functionality of this program will be converted into the base zelda64.py file for use by converter scripts.

## How do I compile the Zelda64 File Reader? ##

* [Zelda64 File Reader Compiling Instructions](https://bitbucket.org/Kjasi/zelda-64-importer/wiki/How to Compile the Zelda64 File Reader)