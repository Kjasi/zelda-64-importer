#pragma once
#include "stdafx.h"
#include <qstring.h>
#include <qlist.h>
#include <qdatastream.h>
#include "vectors.h"
#include <string>
#include <fstream>
#include <vector>
#include <iostream>

enum DataMarkers
{
	DATA_RIG = 0x06,
	DATA_NORMALCOLOR = 0x10,
	DATA_UVS = 0x14
};

enum OpCodes
{
	G_VTX = 0x01,
	G_MODIFYVTX,
	G_TRI1 = 0x05,
	G_TRI2
};

class Weights {
public:
	QString BoneName;
	int BoneID;
	float Value;
};

class Bone {
public:
	QString BoneName;

	int BoneParent, BoneChild, BoneSibling;
	int MeshOffset, MeshOffset_End;

	Vector3f Position;
	Vector3f PoseLocation;

private:
	Vector3f RotationHPB;
	Vector4f RotationQuat;

	void CalculationRotations();

public:
	void read(QByteArray FileData, int offset);

	Vector3f GetRotationHPB() {
		CalculationRotations();
		return RotationHPB;
	}
	Vector4f GetRotationQuat() {
		CalculationRotations();
		return RotationQuat;
	}
};

class Skeleton {
public:
	QString Name;
	QList<Bone> Bones;
};

class Vertex {
public:
	Vector3f Position;
	Vector2f UVs;
	Vector3f Normal;
	Vector4f Color;
	QList<Weights> Weight;

	Vertex(){};

	void read(FILE *f);
	void readAdditional(FILE *f);
	void readWeightData(FILE *f);
};

class Polygon {
public:
	QList<int> VertList;
	int MaterialID;
};

class Animation {

};

class ZObject {
public:
	QList<Vertex> Verts;
	QList<Polygon> Polys;
	QList<Skeleton> Hierarchies;
	QList<Animation> Anims;
	bool Animated;

private:
	QByteArray FileDataQBA;
	std::string FileData;
	//QList<int> RigOffset;

protected:
	//void LocateRigs();
	//void GetRigs();

	std::vector<int> LocateRigs();
	bool GetRigs(const std::vector<int>& rigoffset);

public:
	ZObject();

	void ReadFile(char* filename);
};