// Zelda64Reader.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "zobject.h"
#include "OBJWriter.h"

int main()
{
	ZObject a;

	a.ReadFile("DekuPrincess.zobj");

	OBJWriter(a, "DekuPrincess.obj");

    return 0;
}

