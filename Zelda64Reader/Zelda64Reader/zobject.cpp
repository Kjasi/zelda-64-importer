#include "stdafx.h"
#include "zobject.h"
#include <qfileinfo.h>
#include <qbytearray.h>
#include <qendian.h>

void splitOffset(int inOff, unsigned int &outSeg, unsigned int &outOff)
{
	outSeg = inOff >> 24;
	outOff = inOff & 0x00FFFFFF;
}


// Take Position and PoseLocation to create and update the Rotations
void Bone::CalculationRotations()
{

}

// Read the Bone Data
void Bone::read(QByteArray FileData, int offset)
{
	unsigned int seg, off;
	splitOffset(offset, seg, off);

	int rot_offset = offset & 0xFFFFFF;

	Position.x = FileData.mid(off, 4).toFloat();
	Position.z = FileData.mid(off+2, 4).toFloat();
	Position.y = -FileData.mid(off+4, 4).toFloat();

	BoneChild = FileData.mid(off + 6, 1).toInt();
	BoneSibling = FileData.mid(off + 7, 1).toInt();

	MeshOffset = FileData.mid(off + 8, 4).toULong();
	MeshOffset_End = FileData.mid(off + 12, 4).toULong();

}

// Read Primary Data into Vertex;
void Vertex::read(FILE *f)
{
	/*
	float *fl[4];
	char *c[4];
	float colors[4];

	// Read position
	fread(fl, 2, 3, f);
	Position.x = *fl[0];
	Position.y = -*fl[2];
	Position.z = *fl[1];

	// Read UVs
	fread(fl, 2, 2, f);
	UVs.x = *fl[0];
	UVs.y = *fl[1];

	// Read Vertex Normal
	fread(fl, 1, 3, f);
	Normal.x = 0.00781250 * *fl[0];
	Normal.y = 0.00781250 * *fl[2];
	Normal.z = 0.00781250 * *fl[1];

	// Read Vertex Color
	fread(c, 1, 3, f);
	colors[0] = (int)c[0];
	colors[1] = (int)c[1];
	colors[2] = (int)c[2];

	Color.x = float(colors[0] / 255);
	Color.y = float(colors[1] / 255);
	Color.z = float(colors[2] / 255);
	*/
};

// Used for reading additional vertex Normals, Colors & UVs into a vert
void Vertex::readAdditional(FILE *f)
{
	int data[5];
	fread(data, 4, 1, f);

	if (data[0] == 0x10) {
		// Read Normals

		// Read Colors
	} else if (data[0] == 0x14) {
		// Read UVs
	}
};

void Vertex::readWeightData(FILE * f)
{
};

ZObject::ZObject() {
	Animated = false;
	FileData.clear();
	//RigOffset.clear();
};

/*
void ZObject::LocateRigs()
{

	int j;
	printf("Length of data: %i\n\n", FileData.size());
	for (unsigned int i = 0; i < FileData.size(); i += 4)
	{
		unsigned int di  = FileData.at(i);
		unsigned int di3; //= FileData.at(i + 3);
		char p = FileData.at(i + 3);
		memcpy(&di3, &p, sizeof(int));
		unsigned int di4 = FileData.mid(i + 4, 1).toUInt();
		if (di == DATA_RIG) {
			printf("Data[%i]: %i VS %i, i+3: %i, i+4: %i\n", i, DATA_RIG, di, di3, di4);
		}
		if ((di == DATA_RIG) && ((di3 & 3) == 0) && (di4 != 0)){
			printf("ping 1, %i\n",i);
			unsigned int offset = i + (FileData.mid(i,4).toULong() & 0x00FFFFFF);
			if (offset < FileData.size()) {
				printf("ping 2, %i\n", i);
				unsigned int offset_end = offset + (di4 << 2);
				if (offset_end < FileData.size()) {
					//printf("ping 3, %i\n", i);
					printf("Offset: %i End: %i\n", offset, offset_end);
					for (j = offset; j < offset_end; j += 4) {
						//printf("J: %i\n", j);
						if ((FileData.mid(j, 1).toUInt() != DATA_RIG) || ((FileData.mid(j+3,1).toUInt() & 3) != 0) || (FileData.mid(j,4).toULong() & 0x00FFFFFF > FileData.size()))
						{
							printf("ping 4, break, J: %i\n", j);
							break;
						}
					}

					//printf("ping 5, %i\n", i);
					printf("I VS J: %i, %i\n\n", i, j);

					if (j == i) {
						j |= 0x06000000;
						printf("Rig Found at offset %08X\nRig Found at offset %i\n", j,i);
						RigOffset.push_back(i);
					}
				}
			}
		}
	}

	if (RigOffset.size() == 0) {
		printf("No Rigs Found. :(\n");
	}
}
*/
/*
void ZObject::GetRigs()
{
	if (RigOffset.size() == 0) { return; }

	printf("\n");
	for (int i = 0; i < RigOffset.size(); i++) {
		int j = RigOffset.value(i);
		j |= 0x06000000;
		printf("Rig at Offset: %08X, found %i bones.\n",j,0);
	}
};
*/

std::vector<int> ZObject::LocateRigs()
{
	const std::string& data = FileData;
	std::vector<int> rigoffset;

	//int j;
	std::cout << "length of data: " << data.size() << std::endl;
	for (unsigned int i = 0; i < data.size(); i += 4)
	{
		if(data[i] == 0x06 && (data[i + 3] & 3) == 0 && data[i + 4] != 0)
		{
			unsigned int offset = 0;
			memcpy(&offset, &data[i], sizeof(unsigned int));
			offset = _byteswap_ulong(offset);
			offset &= 0x00FFFFFF;

			//std::cout << "OFFSET:" << offset << ",  Data[" << i << "]: " << DATA_RIG << " VS " << (int)(unsigned char)data[i] << ", i+3: " << (int)(unsigned char)data[i + 3] << ", i+4: " << (int)(unsigned char)data[i + 4] << std::endl;
			
			if (offset < data.size())
			{
				//printf("ping 2, %i\n", i);
				unsigned char tempchar = data[i + 4];
				unsigned int offset_end = offset + (tempchar << 2);

				//printf("Offset: %i End: %i\n", offset, offset_end);
				if (offset_end < data.size())
				{
					unsigned int j = offset;
					
					while (j < offset_end)
					{
						//the problem was, we were working with a file containing little endian data
						unsigned int unpacked_data = 0;
						memcpy(&unpacked_data, &data[j], sizeof(unpacked_data));
						unpacked_data = _byteswap_ulong(unpacked_data);
						unpacked_data &= 0x00FFFFFF;

						//std::cout << "J Data[" << j << "]: " << DATA_RIG << " VS " << (int)(unsigned char)data[j] << ", j+3: " << (int)(unsigned char)data[j + 3] << ", j+4: " << (int)(unsigned char)data[j + 4] << std::endl;

						if (data[j] != 0x06 || (data[j + 3] & 3) != 0 || unpacked_data > data.size())
						{
							//std::cout << "ping 4, break, J: " << j << std::endl;
							break;
						}
						j += 4;

					}
					//std::cout << "I VS J: " << i << "," << j << std::endl;
					if (j == i)
					{
						//re run the program
						j |= 0x06000000;
						std::cout << "   hierarchy found at " << std::hex << j << std::endl;
						std::wcout << "   hierarchy found at " << i << std::endl;
						rigoffset.push_back(i);
					}
				}
			}
		}
	}

	return rigoffset;

}

bool ZObject::GetRigs(const std::vector<int>& rigoffset)
{
	if (rigoffset.size() == 0) { return false; }
	if (FileData.size() == 0) { return false; }

	printf("\n");
	for (size_t i = 0; i < rigoffset.size(); i++) {
		int j = rigoffset[i];
		int jx = j;
		if (j > FileData.size()) { continue; }
		jx |= 0x06000000;

		unsigned int LimbCount = FileData[j + 4];
		printf("Rig at Offset: %i:%08X, found %i bones.\n", j, jx, LimbCount);

		unsigned long LimbIndexOffset;
		memcpy(&LimbIndexOffset, &FileData[j], sizeof(unsigned long));
		LimbIndexOffset = _byteswap_ulong(LimbIndexOffset);

		unsigned int limbIndex_seg, limbiOff;
		splitOffset(LimbIndexOffset, limbIndex_seg, limbiOff);

		printf("Limb Index Offset: %i, LimbiOff: %i, J: %i\n", LimbIndexOffset, limbiOff, j);

		for (size_t b = 0; b < LimbCount; b++) {
			int off = limbiOff + 4 * b;
			unsigned long LimbOffset;
			memcpy(&LimbOffset, &FileData[off], sizeof(unsigned long));
			LimbOffset = _byteswap_ulong(LimbOffset);
			printf("  Limb Rig Offset: %i, offset: %i\n", LimbOffset, off);
		}
	}

	return true;

}

void ZObject::ReadFile(char * filename)
{

	std::ifstream thefile(filename, std::ios::binary);

	if (!thefile)
	{
		printf("File does not exist.\n");
		return;
	}

	// Check file name to see if the file exists
	/*
	QFileInfo finfo(filename);
	if (!finfo.exists()) {
		printf("File does not exist.\n");
		return;
	}
	*/

	// Open the file
	/*
	QFile File(filename);
	if (!File.open(QIODevice::ReadOnly)) {
		printf("Could not open the file.\n");
		return;
	}
	*/

	std::string filedata;

	thefile.seekg(0, std::ios::end);
	size_t total_filesize = thefile.tellg();
	thefile.seekg(std::ios::beg);

	if (total_filesize == 0)
	{
		printf("File is empty.\n");
		return;
	}

	filedata.resize(total_filesize);

	thefile.read(&filedata[0], total_filesize);

	thefile.close();

	// Final Checks
	if (filedata.size() == 0) {
		printf("Could not read the data from the file.\n");
		return;
	}

	FileData = filedata;
	
	// Build our data from what we read
	std::vector<int> rigoffset = LocateRigs();

	if (rigoffset.empty())
	{
		printf("RigsOffset is empty.\n");
		return;
	}

	GetRigs(rigoffset);

	printf("Finished reading file \"%s\".\n", filename);

};
