#pragma once

class Vector2f {
public:
	float x, y;

	Vector2f(float inX = 0.0f, float inY = 0.0f) {
		x = inX;
		y = inY;
	};
};

class Vector3f {
public:
	float x, y, z;

	Vector3f(float inX = 0.0f, float inY = 0.0f, float inZ = 0.0f) {
		x = inX;
		y = inY;
		z = inZ;
	};
};

class Vector4f {
public:
	float x, y, z, w;

	Vector4f(float inX = 0.0f, float inY = 0.0f, float inZ = 0.0f, float inW = 1.0f) {
		x = inX;
		y = inY;
		z = inZ;
		w = inW;
	};
};