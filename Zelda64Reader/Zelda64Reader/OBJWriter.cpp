#include "stdafx.h"
#include <qfile.h>
#include <qtextstream.h>
#include "OBJWriter.h"

void OBJWriter(ZObject Obj, QString Filename)
{
	if (Obj.Verts.size() <=0){ return; }

	QFile outFile(Filename);
	outFile.open(QFile::ReadWrite);

	QTextStream out(&outFile);

	out << "# \n# Zelda64 Object Test File\n# \n" << endl;

	// Write Verts
	out << "# Verts" << endl;
	for (int i = 0; i < Obj.Verts.size(); i++)
	{
		Vertex a = Obj.Verts[i];
		out << "v " << a.Position.x << a.Position.y << a.Position.z << endl;
	}
}